VERSION = 0.0.1
NAME = very-hungry-penguins

PROGS = client server
BINARIES = $(PROGS:=.native) test.native
SRC_DIR = src
TEST_DIR = test
BUILD_DIR = _build
DOC_DIR = doc
CFLAGS = -g -w +A-4-48
ODOCFLAGS = ocamldoc -colorize-code -short-functors -charset utf-8 -stars \
	-I +threads

DESTDIR ?= /usr/local
PREFIX ?= .
SHELL = /bin/bash

PACKAGES = -package lablgtk2 -package alcotest -package unix -package yojson
BUILD = ocamlbuild -use-ocamlfind -tag thread -cflags "$(CFLAGS)" $(PACKAGES)
BUILD_TEST = $(BUILD) -I $(SRC_DIR)

.PHONY: default all test install uninstall deb-pkg clean mrproper distclean doc

default: $(PROGS)

all: $(PROGS) doc test

$(PROGS):
	echo -e "let prefix = \"$(PREFIX)\"\n\
	let path_img = \"$(PREFIX)/img\"" > src/config.ml
	$(BUILD) $(SRC_DIR)/$@.native

test:
	$(BUILD_TEST) $(TEST_DIR)/$@.native
	./$@.native

doc:
	$(BUILD) -ocamldoc "$(ODOCFLAGS)" $(SRC_DIR)/$(NAME).docdir/index.html
	rm -rf $(DOC_DIR)
	mv $(BUILD_DIR)/$(SRC_DIR)/$(NAME).docdir $(DOC_DIR)
	rm -f $(NAME).docdir

install-%: %.native
	mkdir -p $(DESTDIR)/bin/
	cp $< $(DESTDIR)/bin/$<

install: install-client install-server
	mkdir -p $(DESTDIR)/usr/share/$(NAME)/data/{img,test}
	cp img/* $(DESTDIR)/usr/share/$(NAME)/data/img/
	cp test/*.{json,txt} $(DESTDIR)/usr/share/$(NAME)/data/test/

uninstall-%:
	rm -f $(DESTDIR)/bin/$<

uninstall: uninstall-client uninstall-server

deb-pkg: distclean
# we don't want to commit the changelog every time we build the package
	cp debian/changelog .git/
# gbp updates debian/changelog from git commits
# (command from git-buildpackage)
	gbp dch -S -a --git-author --spawn-editor=never
	tar czf ../$(NAME)_$(VERSION).orig.tar.gz --exclude .git .
	debuild --preserve-envvar PATH \
		--set-envvar PREFIX=/usr/share/$(NAME)/data/ \
		-us -uc
	mv .git/changelog debian/changelog

clean:
	rm -rf $(BUILD_DIR)

mrproper: clean
	rm -f $(BINARIES)
	rm -rf $(DOC_DIR)

distclean: mrproper
